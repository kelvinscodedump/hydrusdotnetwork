﻿using System.Reflection.Metadata;
using Microsoft.Data.Sqlite;

namespace CsharpHydrusNetworkLogic
{
    public partial class SqliteAccess
    {
        public void SetFileTag(long fileId, long tagId, bool hasTag)
        {
            using(SqliteCommand command = new()
            {
                Connection = Connection
            })
            {                   
                command.CommandText = $"UPDATE tag_file_mapping SET '{tagId}' = @hasTag WHERE ROWID = @fileId";

                command.Parameters.Add(new()
                {
                    SqliteType = SqliteType.Integer,
                    ParameterName = "@fileId",
                    Value = fileId
                });

                command.Parameters.Add(new()
                {
                    SqliteType = SqliteType.Integer,
                    ParameterName = "@hasTag",
                    Value = hasTag
                });

                command.ExecuteNonQuery();
            }                
        }

        public long CreateTag(string tagText)
        {
            long tagId = InsertData("tags", new()
            {
                new("tag_text", tagText)
            });

            if(tagId != 1)
            {
                AlterTable("tag_file_mapping", new()
                {
                    new($"'{tagId}'", "BIT DEFAULT 0")
                });
            }

            return tagId;
        }

        public long GetTagId(string tagText)
        {
            object? id = Select("ROWID", "tags", true, $"tag_text = '{tagText}'");

            if(id == null)
            {
                return CreateTag(tagText);
            }
            else
            {
                return (long)id;
            }
        }
    }
}

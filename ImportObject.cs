﻿using System.Security.Cryptography;

namespace CsharpHydrusNetworkLogic
{
    public class ImportObject
    {
        public ImportObject(string path)
        {
            Path = path;     
        }

        public string Path { get; set; } = "";

        public string Extension { get; set; } = "";
        public long Size { get; set; } = 0;
        public long Id { get; set; } = 0;
        public byte[]? Hash { get; set; }

        public void GetFileHash()
        {
            int bufferSize = 10485760;
            byte[] hash;

            using(BufferedStream fileStream = new(File.OpenRead(Path), bufferSize))
            {
                using(SHA512 hashSHA512 = SHA512.Create())
                {
                    hash = hashSHA512.ComputeHash(fileStream);
                }
            }

            Hash = hash;
        }

        public void GetInfo()
        {
            FileInfo fileInfo = new(Path);

            Extension = fileInfo.Extension.Trim('.');
            Size = fileInfo.Length;
        }
    }
}

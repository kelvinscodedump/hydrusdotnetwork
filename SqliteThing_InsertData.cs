﻿using Microsoft.Data.Sqlite;

namespace CsharpHydrusNetworkLogic
{
    partial class SqliteAccess
    {

        public long InsertData(string table, string column, object? data, SqliteType type)
        {
            using(SqliteCommand command = new()
            {
                Connection = Connection
            })
            {
                command.CommandText = $"INSERT INTO {table}({column}) VALUES (@data) RETURNING _rowid_";

                SqliteParameter cmdParamater = new()
                {
                    ParameterName = "@data",
                    SqliteType = type,
                    Direction = System.Data.ParameterDirection.Input,
                    Value = data
                };

                command.Parameters.Add(cmdParamater);

                return (long)(command.ExecuteScalar() ?? 0);
            }
        }

        public long InsertData(string table, string column, string data)
        {
            return InsertData(table, column, data, SqliteType.Text);
        }

        public long InsertData(string table, string column, byte[] data)
        {
            return InsertData(table, column, data, SqliteType.Blob);
        }

        public long InsertData(string table, List<DataPair> columnDataCollection)
        {
            using(SqliteCommand command = new()
            {
                Connection = Connection
            })
            {
                string columnString = string.Join(',', columnDataCollection.Select(x => x.ColumnName));
                string dataString = string.Empty;

                for(int index = 0; index < columnDataCollection.Count; index++)
                {
                    DataPair columnInsert = columnDataCollection[index];

                    if(index != 0)
                    {
                        dataString += ", ";
                    }

                    dataString += $"@data{index}";

                    command.Parameters.Add(new()
                    {
                        ParameterName = $"@data{index}",
                        SqliteType = columnInsert.Type,
                        Direction = System.Data.ParameterDirection.Input,
                        Value = columnInsert.Data
                    });

                }

                command.CommandText = $"INSERT INTO {table}({columnString}) VALUES({dataString}) RETURNING ROWID";

                return (long)(command.ExecuteScalar() ?? 0L);
            }
        }

        public class DataPair
        {
            // Main Constructor

            public DataPair(string columnName, object data)
            {
                Data = data;
                ColumnName = columnName;
            }

            // Constructor Overloads

            public DataPair(string columnName, string data) : this(columnName, (object)data)
            {
                Type = SqliteType.Text;
            }

            public DataPair(string columnName, long data) : this(columnName, (object)data)
            {
                Type = SqliteType.Integer;
            }

            public DataPair(string columnName, byte[] data) : this(columnName, (object)data)
            {
                Type = SqliteType.Blob;
            }

            public DataPair(string columnName, int data) : this(columnName, (long)data) { }

            public DataPair(string columnName, short data) : this(columnName, (long)data) { }

            public DataPair(string columnName, byte data) : this(columnName, (long)data) { }

            public DataPair(string columnName, bool data) : this(columnName, (long)(data ? 1 : 0)) { }

            public DataPair(string columnName, DateTime data) : this(columnName, data.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff")) { }

            // Properties and Fields

            public string ColumnName { get; set; }
            public object? Data { get; set; }
            public SqliteType Type { get; set; }
        }
    }
}

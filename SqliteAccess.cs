﻿using Microsoft.Data.Sqlite;

namespace CsharpHydrusNetworkLogic
{
    public partial class SqliteAccess
    {
        // Constructor

        public SqliteAccess(string fMainDbSrc)
        {
            FilesMainDbSource = fMainDbSrc;
            Connection = new($"Data Source={FilesMainDbSource}");
        }

        // Properties

        public SqliteConnection Connection { get; set; }

        private string _filesMainDbSource = string.Empty;
        public string FilesMainDbSource
        {
            get => _filesMainDbSource;
            set
            {
                if(!File.Exists(value))
                {
                    _ = File.Create(value);
                }

                _filesMainDbSource = value;
            }
        }       

        public void CreateTable(string tableName, List<(string, string)> columns)
        {
            using(SqliteCommand command = new()
            {
                Connection = Connection
            })
            {
                string columnString = string.Join(',', columns.Select(x => $"{x.Item1} {x.Item2}"));
                command.CommandText = $"CREATE TABLE IF NOT EXISTS {tableName}({columnString})";
                command.ExecuteNonQuery();
            }
        }

        public void AlterTable(string tableName, List<(string, string)> columns)
        {
            using(SqliteCommand command = new()
            {
                Connection = Connection
            })
            {
                string columnString = string.Join(',', columns.Select(x => $"{x.Item1} {x.Item2}"));
                command.CommandText = $"ALTER TABLE {tableName} ADD {columnString}";
                command.ExecuteNonQuery();
            }
        }

        public void CheckAndCreateFileTable()
        {
            CreateTable("files", new()
            {
                ("hash","BLOB NOT NULL"),
                ("import_date", "TEXT NOT NULL"),
                ("extension", "TEXT NOT NULL"),
                ("file_size", "INTEGER NOT NULL")
            });
        }

        public void CheckAndCreateTagTable()
        {
            CreateTable("tags", new()
            {
                ("namespace_id", "INTEGER"),
                ("tag_text","TEXT NOT NULL"),
            });
        }

        public void CheckAndCreateTagMappingTable()
        {
            CreateTable("tag_file_mapping", new()
            {
                ("'1'","BIT DEFAULT 0")
            });
        }
              
        public long GetCount(string table, object obj)
        {
            using(SqliteCommand command = new()
            {
                Connection = Connection
            })
            {
                command.CommandText = $"SELECT ROWID FROM {table}";
                command.CommandText += " WHERE hash = @data";

                SqliteParameter cmdParamater = new()
                {
                    ParameterName = "@data",
                    Direction = System.Data.ParameterDirection.Input,
                    SqliteType = SqliteType.Blob,
                    Value = obj
                };

                command.Parameters.Add(cmdParamater);

                return (long)(command.ExecuteScalar() ?? 0L);
            }
        }

        public object? Select(string srcColumn, string table, bool onlyFirst = false, string? condition = null)
        {
            using(SqliteCommand command = new()
            {
                Connection = Connection
            })
            {
                command.CommandText = $"SELECT {srcColumn} FROM {table}";
                if(condition != null)
                {
                    command.CommandText += $" WHERE {condition}";
                }

                if(onlyFirst)
                {
                    return command.ExecuteScalar();
                }
                else
                {
                    return null;
                }
            }
        }
    }
}


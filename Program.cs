﻿using System.Collections.Concurrent;
using CsharpHydrusNetworkLogic;
using static CsharpHydrusNetworkLogic.SqliteAccess;

public static class MainClass
{
    const string filesMainDb = @"C:\Users\Admin\source\repos\CsharpHydrusNetworkCommon\db\files_main.db";
    private static readonly string[] exts = { "png", "jpg", "webp", "mp4", "webm", "gif", "mov" };

    private static SqliteAccess? sqliteAccess { get; set; }

    public static void Main()
    {
        sqliteAccess = new(filesMainDb);
        sqliteAccess.Connection.Open();

        sqliteAccess.CheckAndCreateFileTable();
        sqliteAccess.CheckAndCreateTagTable();
        sqliteAccess.CheckAndCreateTagMappingTable();

        bool isExit = false;

        while(!isExit)
        {
            string input = Console.ReadLine() ?? string.Empty;

            long tagId;
            long fileId;
            string tagText;

            switch(input.ToLower())
            {
                case "exit":
                    isExit = true;
                    break;

                case "add files":
                    Run();
                    break;

                case "create tag":
                    string inputCmdParameter = Console.ReadLine() ?? string.Empty;
                    if(string.IsNullOrEmpty(inputCmdParameter))
                    {
                        break;
                    }
                    tagId = sqliteAccess.GetTagId(inputCmdParameter);
                    Console.WriteLine(tagId);
                    break;

                case "add tag to file":
                    fileId = long.Parse(Console.ReadLine());
                    tagText = Console.ReadLine() ?? string.Empty;
                    if(string.IsNullOrEmpty(tagText))
                    {
                        break;
                    }
                    tagId = sqliteAccess.GetTagId(tagText);
                    sqliteAccess.SetFileTag(fileId, tagId, true);
                    Console.WriteLine(tagId);
                    break;

                case "remove tag from file":
                    fileId = long.Parse(Console.ReadLine());
                    tagText = Console.ReadLine() ?? string.Empty;
                    if(string.IsNullOrEmpty(tagText))
                    {
                        break;
                    }
                    tagId = sqliteAccess.GetTagId(tagText);
                    sqliteAccess.SetFileTag(fileId, tagId, false);
                    Console.WriteLine(tagId);
                    break;
            }
        }
    }

    internal static void Run()
    {
        string inputPath = @"C:\Users\Admin\source\repos\CsharpHydrusNetworkCommon\inFiles\";
        string outputPath = @"C:\Users\Admin\source\repos\CsharpHydrusNetworkCommon\dbfiles\";
        int dupeCounter = 0;

        List<ImportObject> paths = new();
        ConcurrentBag<ImportObject> values = new();
        ParallelOptions praOpts = new()
        {
            MaxDegreeOfParallelism = 12
        };

        ParallelLoopResult result = new();

        foreach(string ext in exts)
        {
            paths.AddRange(Directory.EnumerateFiles(inputPath, $"*.{ext}", SearchOption.AllDirectories).Select(x => new ImportObject(x)).ToList());
        }

        Task hashingTask = Task.Run(() =>
        {
            result = Parallel.For(0, paths.Count, i =>
            {
                ImportObject x = paths[i];

                x.GetFileHash();
                values.Add(x);
            });

        });

        Task sqliteDataInsertTask = Task.Run(() =>
        {
            try
            {
                while(!result.IsCompleted || !values.IsEmpty)
                {
                    if(values.TryTake(out ImportObject? item))
                    {
                        bool isDupe = sqliteAccess.GetCount("files", item.Hash) > 0;

                        if(!isDupe)
                        {
                            item.GetInfo();

                            item.Id = sqliteAccess.InsertData("files", new List<DataPair>()
                            {
                                new("hash", item.Hash),
                                new("extension", item.Extension),
                                new("file_size", item.Size),
                                new("import_date", DateTime.Now)
                            });

                            sqliteAccess.InsertData("tag_file_mapping", new List<DataPair>()
                            {
                                new("ROWID", item.Id)
                            });

                            File.Copy(item.Path, $"{outputPath}{item.Id}.{item.Extension}", true);
                        }
                        else
                        {
                            dupeCounter++;
                        }
                    }
                }

            }
            catch(Exception ex)
            {
                throw;
            }
        });

        sqliteDataInsertTask.Wait();
        Console.WriteLine($"Done. Dupes: {dupeCounter} out of {paths.Count}");
    }
}
